## Sujet du projet

La révolution numérique conduit à une croissance exponentielle des documents
multimédia (texte, images, audio, vidéo). Parmi des contenus, les données de type image sont
ubiquitaires et doivent faire l’objet d’une indexation qui n’est plus seulement textuelle, mais
qui s’appuie sur des descripteurs (features) qui en sont extraits automatiquement. On parle
alors d’indexation par le contenu (Content-Based Image Retrieval - CBIR). Pour réaliser ce
type de traitement automatique, il convient de procéder à la classification des images, qui
consiste à extraire suffisamment d’information des primitives de l’image pour pouvoir la
catégoriser (ex : chien, chat, voiture, etc.)

La classification est généralement réalisée de manière supervisée. Dans un premier
temps, on sélectionne comme référence un (petit) ensemble d’images déjà labellisées (on
connaît leur classes d’appartenance) et représentatives, dont on extrait des primitives (couleur,
texture, formes) : c’est l’ensemble d’apprentissage (learning set). Dans un second temps, on
détermine grâce à cet ensemble un modèle de décision, en faisant en sorte que celui-ci soit
suffisamment robuste et conduise aussi à de bonnes décisions dans le cas de données déjà
labellisées, mais qui n’ont pas servi à bâtir le modèle de décision : c’est l’ensemble de
validation (validation set). Enfin, quand le modèle a été jugé suffisamment robuste, il peut
être appliqué sur un jeu de test (test set), contenant des exemples distincts des deux premiers
ensembles. 

Le but de ce projet est de mettre en œuvre deux approches de la classification d’images. <br>
Cette partie, la Partie 1, consiste à extraire des attributs (primitives) classiquement
utilisés en traitement d’image, comme la couleur, la texture, etc., et de procéder à une
classification d’image s’appuyant sur des critères de décision simples, basés sur les distances
entre vecteurs d’attributs.

## Outils de développement utilisés

Pour ce projet, nous avions à utiliser python. J'ai utilisé la distribution Anaconda, qui permet de créer facilement des environnements virtuels en python. L'environnement de développement que j'ai utilisé est Spyder, un IDE dédié au développement sur python. Le code a ensuite été mis, comme vous pourrez le voir dans la suite, dans des fichiers Notebook Jupyter, ce qui permet de commenter directement le code.

## Les deux parties

Rapport partie 1
https://gitlab.com/Baobax/traitementimage/blob/master/Partie1.ipynb

Rapport partie 2
https://gitlab.com/Baobax/traitementimage/blob/master/Partie2.ipynb
